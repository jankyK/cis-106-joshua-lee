import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

class Main {
  public static void main(String[] args) {
    ArrayList<String> studentInfo = new ArrayList<String>();
    String[] studentFiles = new String[3];
    int[] studentGrades = new int[3];
    int[] sortedGrades = new int[3];
    studentInfo = getFile();

    for(int i = 0; i < studentInfo.size(); i++){
      studentFiles[i] = studentInfo.get(i);
    }
    studentGrades = parseInfo(studentFiles);
    sortedGrades = sortGrades(studentGrades);
    outputResults(sortedGrades);
  }

  public static ArrayList<String> getFile(){
    String fileName = "GradeFile.txt";
    String line = null;
    ArrayList<String> studentGrades = new ArrayList<String>();
      try {
          FileReader fileReader = new FileReader(fileName);

          BufferedReader bufferedReader = new BufferedReader(fileReader);

          while((line = bufferedReader.readLine()) != null) {
              studentGrades.add(line);
          }   

          bufferedReader.close();         
      }
      catch(FileNotFoundException ex) {
        System.out.println("Unable to open file '" + fileName + "'");               
        }
      catch(IOException ex) {
        System.out.println("Error reading file '" + fileName + "'");
      }

      return studentGrades;
  }

  public static int[] parseInfo(String[] name){
    String[] splitInfo = new String[2];
    int[] studentGrades = new int[3];
    int studentGrade;
    for(int i = 0; i < name.length; i++){
      splitInfo = name[i].split(": ");
      studentGrade = Integer.parseInt(splitInfo[1]);   
      studentGrades[i] = studentGrade;
    }
    
    return studentGrades;
  }

  public static int[] sortGrades(int[] grades){
    int[] intGrades = new int[grades.length];
    for(int i = 0; i < grades.length; i++){
      intGrades[i] = grades[i];
    }
    Arrays.sort(intGrades);
    return intGrades;
  }

  public static void outputResults(int[] sortedGrades){
    for(int i = 0; i < sortedGrades.length; i++){
      System.out.println(sortedGrades[i]);
    }
    System.out.println("Highest Grade: " + sortedGrades[2]);
    System.out.println("Lowest Grade: " + sortedGrades[0]);
    System.out.println("Average Grade: " + (sortedGrades[0]+sortedGrades[1]+sortedGrades[2])/3);
  }
}