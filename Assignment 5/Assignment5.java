// This program calls GetAge which is a method that gets input from the user to figure 
//out the user's age. From GetAge, ConvertAge is called which converts the age to age in 
//months, days, hours and seconds. ConvertAge then calls OutputResults which takes 
//parameters from ConvertAge and outputs the results.
import java.util.*;
import java.lang.Math;

public class JavaApplication {
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        int age;
        double ageMonths;
        double ageDays;
        double ageHours;
        double ageSeconds;
        
        age = getAge();
        ageMonths = convertMonths(age);
        ageDays = convertDays(age);
        ageHours = convertHours(age);
        ageSeconds = convertSeconds(age);
        outputResults(ageMonths, ageDays, ageHours, ageSeconds);
    }
    
    public static double convertDays(int age) {
        double days;
        
        days = age * 30;
        
        return days;
    }
    
    public static double convertHours(int age) {
        double hours;
        
        hours = age * 24;
        
        return hours;
    }
    
    public static double convertMonths(int age) {
        double months;
        
        months = age * 12;
        
        return months;
    }
    
    public static double convertSeconds(int age) {
        double seconds;
        
        seconds = age * 3600;
        
        return seconds;
    }
    
    public static int getAge() {
        int age;
        
        System.out.println("Enter age in years: ");
        age = input.nextInt();
        
        return age;
    }
    
    public static void outputResults(double ageMonths, double ageDays, double ageHours, double ageSeconds) {
        int age;
        
        age = ageMonths / 12;
        System.out.println("Age: " + age + " Age in months: " + ageMonths + " Age in days: " + ageDays + " Age in hours: " + ageHours + " Age in seconds: " + ageSeconds);
    }
}

