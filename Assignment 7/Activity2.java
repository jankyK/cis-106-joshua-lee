import java.util.*;
import java.lang.Math;

public class JavaApplication {
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        double hours;
        double hourlyRate;
        double grossPay;
        
        hours = getHours();
        hourlyRate = getRate();
        grossPay = computePay(hours, hourlyRate);
        outputPay(grossPay);
    }
    
    public static double computePay(double hours, double rate) {
        double grossPay;
        double oTHours;
        
        if (hours > 40) {
            oTHours = hours - 40;
            grossPay = 40 * rate + rate * 1.5 * oTHours;
            System.out.println("Overtime worked: " + oTHours);
        } else {
            grossPay = hours * rate;
        }
        
        return grossPay;
    }
    
    public static double getHours() {
        double hrsWorked;
        
        System.out.println("Enter amount of hours worked: ");
        hrsWorked = input.nextDouble();
        
        return hrsWorked;
    }
    
    public static double getRate() {
        double hrRate;
        
        System.out.println("Enter amount paid per hour: ");
        hrRate = input.nextDouble();
        
        return hrRate;
    }
    
    public static void outputPay(double gross) {
        System.out.println("Gross pay: " + gross);
    }
}
