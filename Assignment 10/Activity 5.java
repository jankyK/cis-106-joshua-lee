import java.util.*;
import java.lang.Math;

class Main {
private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        int guesses;
        guesses = guessNum();
        outputResult(guesses);
    }
    
    public static int guessNum() {
        int numGuess;
        int guessHolder;
        int lowBound;
        int highBound;
        boolean correctBool;
        String userResponse;
        
        numGuess = 0;
        guessHolder = 50;
        lowBound = 0;
        highBound = 100;
        correctBool = false;
        System.out.println("Think of a number from 0 to 100. I will try and guess that number now.");
        
        while(correctBool == false){
          System.out.println("Here's my guess: " + guessHolder + ". Was I right? Enter 'H' if it is higher, 'L' if it is lower, or 'E' if I guessed correctly");
          numGuess+=1;
          userResponse = input.next();
          
          if(userResponse.equals("H")){
            lowBound = guessHolder;
            guessHolder += (highBound-lowBound)/2;
            if(guessHolder < lowBound){
              guessHolder = lowBound;
            }
            else if(guessHolder == 99 && userResponse.equals("H")){
              guessHolder = 100;
            }
          }
          else if(userResponse.equals("L")){
            highBound = (guessHolder);
            guessHolder -= (highBound-lowBound)/2;
            if(guessHolder > highBound){
              guessHolder = highBound;
            }
          }
          else if(userResponse.equals("E")){
            System.out.println("Hooray!");
            correctBool = true;
          }
        };
        
        return numGuess;
    }
    public static void outputResult(int numGuess){
      System.out.println("Number of guesses: " + numGuess);
    }
}
