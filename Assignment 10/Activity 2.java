import java.util.*;
import java.lang.Math;

class Main {
private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        int userValue;
        int userIter;
        
        userValue = getValue();
        userIter = getIterations();
        forTable(userValue, userIter);
        WhileTable(userValue, userIter);
    }
    
    public static void forTable(int value, int iterations) {
        int placeHolder;
        int expressionHolder;
        
        placeHolder = 1;
        
        for (placeHolder = 1; placeHolder<=iterations; placeHolder++){
          expressionHolder = value * placeHolder;
            System.out.println("Expression: " + value + " x " + placeHolder + " = " + expressionHolder);
        }
    }
    public static void WhileTable(int value, int iterations){
    int placeHolder;
    int expressionHolder;
    
    placeHolder = 1;
        while (placeHolder <= iterations) {
            expressionHolder = value * placeHolder;
            System.out.println("Expression: " + value + " x " + placeHolder + " = " + expressionHolder);
            placeHolder += 1;
        }
    }
    
    public static int getIterations() {
        int iterations;
        
        System.out.println("Enter amount of iterations: ");
        iterations = input.nextInt();
        
        return iterations;
    }
    
    public static int getValue() {
        int value;
        
        System.out.println("Enter value: ");
        value = input.nextInt();
        
        return value;
    }
}