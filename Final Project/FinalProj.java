import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.lang.Math;

class Main {

  public static void main(String argv[]) {
    String url = "https://www.w3schools.com/xml/simple.xml";
    ArrayList<String> foodName = new ArrayList<String>(5);
    ArrayList<String> foodPrice = new ArrayList<String>(5);
    ArrayList<String> foodDesc = new ArrayList<String>(5);
    ArrayList<String> foodCals = new ArrayList<String>(5);
    double avgPrice = 0;
    double avgCalories = 0;
    try{
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(new URL(url).openStream());
      NodeList nList = doc.getElementsByTagName("food");

      foodName = getNames(url, dbFactory, dBuilder, doc, nList);
      foodPrice = getPrices(url, dbFactory, dBuilder, doc, nList);
      foodDesc = getDesc(url, dbFactory, dBuilder, doc, nList);
      foodCals = getCals(url, dbFactory, dBuilder, doc, nList);

      avgPrice = calcAverage(foodPrice);
      avgCalories = calcAverage(foodCals);
    }
    catch (Exception e) {
	    e.printStackTrace();
    }
    
    int round = (int) Math.round(avgPrice * 100);
    double finalAvgPrice = round/100.0;
    outputResults(foodName, foodPrice, foodDesc, foodCals, finalAvgPrice, avgCalories);
    
  } 

  public static ArrayList<String> getNames(String url, DocumentBuilderFactory dbFactory, DocumentBuilder dBuilder, Document doc, NodeList nList){
    ArrayList<String> foodNames = new ArrayList<String>();
    
    try{
	    doc = dBuilder.parse(new URL(url).openStream());
      nList = doc.getElementsByTagName("food");

      for (int i = 0; i < nList.getLength(); i++) {
		    Node nNode = nList.item(i);
			
		    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
          String tempStr;

			    Element eElement = (Element) nNode;
          tempStr = eElement.getElementsByTagName("name").item(0).getTextContent();
          foodNames.add(tempStr);
		    }
	    }
    }
    catch (Exception e) {
	    e.printStackTrace();
    }

    return foodNames;
  }

  public static ArrayList<String> getPrices(String url, DocumentBuilderFactory dbFactory, DocumentBuilder dBuilder, Document doc, NodeList nList){
    ArrayList<String> foodPrices = new ArrayList<String>();
    try{
	    doc = dBuilder.parse(new URL(url).openStream());
      nList = doc.getElementsByTagName("food");
      
      for (int i = 0; i < nList.getLength(); i++) {
		    Node nNode = nList.item(i);
			
		    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
          String tempStr;

			    Element eElement = (Element) nNode;
          tempStr = eElement.getElementsByTagName("price").item(0).getTextContent();
          foodPrices.add(tempStr);
		    }
	    }
      
    }
    catch (Exception e) {
	    e.printStackTrace();
    }

    return foodPrices;
  }

  public static ArrayList<String> getDesc(String url, DocumentBuilderFactory dbFactory, DocumentBuilder dBuilder, Document doc, NodeList nList){
    ArrayList<String> foodDesc = new ArrayList<String>();
    try{
	    doc = dBuilder.parse(new URL(url).openStream());
      nList = doc.getElementsByTagName("food");
      
      for (int i = 0; i < nList.getLength(); i++) {
		    Node nNode = nList.item(i);
			
		    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
          String tempStr;

			    Element eElement = (Element) nNode;
          tempStr = eElement.getElementsByTagName("description").item(0).getTextContent();
          foodDesc.add(tempStr);
		    }
	    }
      
    }
    catch (Exception e) {
	    e.printStackTrace();
    }

    return foodDesc;
  }
  public static ArrayList<String> getCals(String url, DocumentBuilderFactory dbFactory, DocumentBuilder dBuilder, Document doc, NodeList nList){
    ArrayList<String> foodCals = new ArrayList<String>();
    try{
	    doc = dBuilder.parse(new URL(url).openStream());
      nList = doc.getElementsByTagName("food");
      
      for (int i = 0; i < nList.getLength(); i++) {
		    Node nNode = nList.item(i);
			
		    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
          String tempStr;

			    Element eElement = (Element) nNode;
          tempStr = eElement.getElementsByTagName("calories").item(0).getTextContent();
          foodCals.add(tempStr);
		    }
	    }
      
    }catch (Exception e) {
	  e.printStackTrace();
    }
    return foodCals;
  }

  public static void outputResults(ArrayList<String> names, ArrayList<String> price, ArrayList<String> desc, ArrayList<String> cals, double avgPrice, double avgCalories){
    for(int i = 0; i < names.size(); i ++){
      System.out.println(names.get(i) + " - " + price.get(i) + " - " + desc.get(i) + " - " + cals.get(i) +" Calories");
      System.out.println("--------------------------------------------------");
    }
    System.out.println(names.size() + " Items - Average Price: " + avgPrice + " - Average Calories: " + avgCalories);
  }

  public static Double calcAverage(ArrayList<String> info){
    String tempStr;
    double total = 0;
    double average = 0;

    for(int i = 0; i < info.size(); i++){
      tempStr = info.get(i);
      if(tempStr.contains("$")){
        String newString = tempStr.replaceAll("[^\\d.]+", "");
        double temp = Double.parseDouble(newString);
        total += temp;
      }
      else{
        tempStr = info.get(i);
        double temp2 = Double.parseDouble(tempStr);
        total += temp2;
      }
      
    }
    average = total/info.size();
    return average;
  }
}
