
import java.util.*;
import java.lang.Math;

class Main {
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        int amtTests;
        double whileTestTotal;
        double forTestTotal;
        double forTestAvg;
        double whileTestAvg;
        
        amtTests = getNumTests();
        forTestTotal = forScores(amtTests);
	      whileTestTotal = whileScores(amtTests);
        forTestAvg = calcAverage(amtTests, whileTestTotal);
        whileTestAvg = calcAverage(amtTests, forTestTotal);

        outputResults(forTestAvg);
        outputResults(whileTestAvg);
    }
    
    public static double calcAverage(int amtTests, double testTotal) {
        double testAvg;
        
        testAvg = testTotal / amtTests;
        
        return testAvg;
    }
    
    public static int getNumTests() {
        int numTests;
        
        System.out.println("Enter amount of scores to enter: ");
        numTests = input.nextInt();
        
        return numTests;
    }
    
    public static double forScores(int amtTests) {
        double testTotal;
        int holder;
        int count;
        
        count = 0;
        testTotal = 0;
        for (count = 0; count <= amtTests - 1; count++) {
            System.out.println("Enter test score: ");
            holder = input.nextInt();
            testTotal = holder + testTotal;
        }
        return testTotal;
    }

    public static int whileScores(int amtTests) {
	int testTotal = 0;
	int holder;
	int count = 0;
	while (count < amtTests) {
            System.out.println("Enter test score: ");
            holder = input.nextInt();
            testTotal = holder + testTotal;
            count = count + 1;
        }
        return testTotal;
}

    public static void outputResults(double testAvg) {
        System.out.println("Average of tests: " + testAvg);
    }
}
