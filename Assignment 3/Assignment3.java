//This program takes input from the user as HrW as hours worked. Then it stores the next input as the hourly rate in HrRate. 
//The next output defines the gross pay by multiplying HrW and HrRate.
import java.util.*;
import java.lang.Math;

public class JavaApplication {
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        int hrW;
        double hrRate;
        
        System.out.println("Enter amount of hours worked: ");
        hrW = input.nextInt();
        System.out.println("Enter hourly rate: ");
        hrRate = input.nextDouble();
        System.out.println("Gross pay: " + hrW * hrRate);
    }
}
