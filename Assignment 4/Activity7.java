import java.util.*;
import java.lang.Math;

public class JavaApplication {
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        int bYear;
        int bMonth;
        int bDay;
        int dOW;
        
        System.out.println("Enter birth year: ");
        bYear = input.nextInt();
        System.out.println("Enter birth month in numerical form: ");
        bMonth = input.nextInt();
        System.out.println("Enter birth day: ");
        bDay = input.nextInt();
        if (bYear + 4 == 0) {
            if (bMonth >= 2) {
                dOW = (bMonth - 1) * 30 + 28 + bDay + 7;
            } else {
                dOW = bMonth * 30 + bDay + 7;
            }
        } else {
            dOW = bDay + 13 * (bMonth + 1) / 5 + (bYear + 100) + 5 - bYear / 100 + 7;
        }
        if (dOW == 0) {
            System.out.println("Date of birth: Sunday");
        } else {
            if (dOW == 1) {
                System.out.println("Date of birth: Monday");
            } else {
                if (dOW == 2) {
                    System.out.println("Date of birth: Tuesday");
                } else {
                    if (dOW == 3) {
                        System.out.println("Date of birth: Wednesday");
                    } else {
                        if (dOW == 4) {
                            System.out.println("Date of birth: Thursday");
                        } else {
                            if (dOW == 5) {
                                System.out.println("Date of birth: Friday");
                            } else {
                                if (dOW == 6) {
                                    System.out.println("Date of birth: Saturday");
}}}}}}}}}