/* Missing comments here ... */

import java.util.*;
import java.lang.Math;

public class JavaApplication {
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        double miles;
        double feet;
        double inches;
        double yards;
        
        System.out.println("Enter miles to convert: ");
        miles = input.nextDouble();
        
        yards = miles * 1760;
        feet = yards * 3;
        inches = feet * 12;
        
        System.out.println("Miles: " + miles);
        System.out.println("Yards: " + yards);
        System.out.println("Feet: " + feet);
        System.out.println("Inches: " + inches);
    }
}