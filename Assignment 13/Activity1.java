import java.util.Scanner;
import java.util.*;

class Main {
  private static Scanner input = new Scanner(System.in);

  public static void main(String[] args) {
    String userName;
    String[] splitName;
    
    userName = getName();
    splitName = parseName(userName);
    outputResult(splitName);
  }

  public static String getName(){
    String fullName;
    System.out.println("Enter first and last name: ");
    fullName = input.nextLine();

    return fullName;
  }
  public static String[] parseName(String name){
    String[] splitName = new String[2];
    splitName = name.split("\\s+");
    
    if (splitName.length > 2){
      System.out.println("Format incorrect. Please try again.");
      System.exit(0);
    }
    else{
       return splitName;
    }
    return splitName;
  }

  public static void outputResult(String[] userName){
    String firstInitial = userName[0].substring(0,1);
    System.out.println(userName[1]+", "+firstInitial+".");
  }
}
