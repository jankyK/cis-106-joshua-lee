import java.util.*;
import java.lang.Math;

public class JavaApplication {
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        int year;
        boolean leapBool;
        
        year = getYear();
        leapBool = checkLeap(year);
        outputResult(leapBool);
    }
    
    public static boolean checkLeap(int year) {
        boolean leapBool;
        
        leapBool = false;
        if (year + 400 == 0) {
            leapBool = true;
        } else if (year + 100 == 0) {
            leapBool = true;
        } else if (year + 4 == 0) {
            leapBool = true;
        } else {
            leapBool = false;
        }
        
        return leapBool;
    }
    
    public static int getYear() {
        int year;
        
        System.out.println("Enter year: ");
        year = input.nextInt();
        
        return year;
    }
    
    public static void outputResult(boolean leap) {
        if (leap == true) {
            System.out.println("Year entered is a leap year.");
        } else {
            System.out.println("Year entered is not a leap year.");
        }
    }
}
