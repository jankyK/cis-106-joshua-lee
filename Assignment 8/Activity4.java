import java.util.*;
import java.lang.Math;

public class JavaApplication {
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        double userDistance;
        
        userDistance = getDistance();
        getConversions(userDistance);
    }
    
    public static void getConversions(double dist) {
        String convType;
        double conv1;
        double conv2;
        double conv3;
        
        System.out.println("Please enter which conversion type is preferred, US or Metric: ");
        convType = input.nextLine();
        if (convType == "Metric" || convType == "metric") {
            convType = "Metric";
            conv1 = dist * 1.60934;
            conv2 = dist * 1609.34;
            conv3 = dist * 160934;
        } else if (convType == "US" || convType == "us" || convType == "United States" || convType == "united states") {
            convType = "US";
            conv1 = dist * 1760;
            conv2 = dist * 5280;
            conv3 = dist * 63360;
        } else {
            System.out.println("Please try again.");
        }
        outputResults(conv1, conv2, conv3, convType);
    }
    
    public static double getDistance() {
        double userDist;
        
        System.out.println("Enter a distance in miles: ");
        userDist = input.nextDouble();
        
        return userDist;
    }
    
    public static void outputResults(double con1, double con2, double con3, String conType) {
        if (conType == "US") {
            System.out.println("Conversions are as follows: ");
            System.out.println("Conversion 1: " + con1 + " yards");
            System.out.println("Conversion 2: " + con2 + " feet");
            System.out.println("Conversion 3: " + con3 + " inches");
        } else {
            System.out.println("Conversions are as follows: ");
            System.out.println("Conversion 1: " + con1 + " kilometers");
            System.out.println("Conversion 2: " + con2 + " meters");
            System.out.println("Conversion 3: " + con3 + " centimeters");
        }
    }
}
