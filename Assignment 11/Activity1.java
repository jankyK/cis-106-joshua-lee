//This code asks the user to enter the amount of scores that they want to average out. Then it passes the number designated by the user into a method
//that uses it as a loop control variable to get each individual test score and add them into an array. The output function also serves to find out the min
//and max score and display all the results.

import java.util.*;
import java.lang.Math;

class Main{
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        int numScores;
        int[] gradeList;
        double averageScore;
        
        numScores = getNumScores();
        gradeList = enterScores(numScores);
        averageScore = getAverage(gradeList);
        outputResults(gradeList, averageScore);
    }
    
    public static int[] enterScores(int numScores) {
        int gradeHolder;
        int[] gradeList = new int[numScores];

        for (int i = 0; i <= numScores - 1; i++) {
            System.out.println("Enter score: ");
            gradeHolder = input.nextInt();
            gradeList[i] = gradeHolder;
        }
        
        return gradeList;
    }

    public static double getAverage(int[] gradeList) {
        double avgScore = 0;

        for (int i = 0; i < gradeList.length; i++){
          avgScore += gradeList[i];
        }
        avgScore = avgScore/gradeList.length;
        return avgScore;
    }
    
    public static int getNumScores() {
        int numScores;
        
        System.out.println("Enter amount of tests to average: ");
        numScores = input.nextInt();
        
        return numScores;
    }

    public static void outputResults(int[] gradeList, double avgScore){
      int max = gradeList[0];
      int min = gradeList[0];

      for (int i = 1; i < gradeList.length; i++){
        if (gradeList[i] > max){
          max = gradeList[i];
        }
      }
      for (int j = 1; j < gradeList.length; j++){
        if (gradeList[j] < min){
          min = gradeList[j];
        }
      }
      System.out.println("Average test score: " + avgScore);
      System.out.println("Highest test score: " + max);
      System.out.println("Lowest test score: " + min);
    }
}