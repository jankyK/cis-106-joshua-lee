import java.util.*;
import java.lang.Math;

class Activity2 {
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        int year;
        int month;
        String name;
        int days;

        year = getYear();
        while (true) {
            month = getMonth();
            if (month < 1 || month > 12) {
                break;
            }
            name = monthName(month);
            days = monthDays(year, month);
            displayResults(name, days);
        }
    }

    public static int getYear() {
        System.out.println("Enter year to check for leap year: ");
        int year = input.nextInt();
        return year;
    }
    
    public static int getMonth() {
        System.out.println("Enter month to check amount of days: ");
        int month = input.nextInt();
        return month;
    }

    public static boolean leapCheck(int year) {
        boolean leapYear = false;
        if (year % 400 == 0) {
            leapYear = true;
        } else if (year % 100 == 0) {
            leapYear = false;
        } else if (year % 4 == 0) {
            leapYear = true;
        } else {
            leapYear = false;
        };
        return leapYear;
    }

    public static String monthName(int month) {
        String[] months = {
            "January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        };
        
        return months[month - 1];
    }

    public static int monthDays(int year, int month) {
        int[] days = {
            31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
        };

        if (leapCheck(year)) {
            days[1] = 29;
        }
        
        return days[month - 1];
    }
    
    public static void displayResults(String name, int days) {
        System.out.println(String.format("%s has %d days.", name, days));
    }
}
