//This program sets up the game in a separate function by assigning integers to the spots in an array. After returning the array,
//the game begins in Main where the user has a choice of 3 doors. Remaining tries are outputted if they chose a goat, and breaks if they win.
import java.util.*;
import java.lang.Math;

class Activity4{
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        int[] doors;
        int userChoice;
        int montyChoice;
        int swapChoice;
        int firstWin = 0;
        int changedAnswer = 0;
  
        for(int i = 0; i < 100; i++){
            doors = setGame();
            userChoice = getChoice();
            montyChoice = getMonty(doors, userChoice);
            swapChoice = getSwap(userChoice, montyChoice);

            if(doors[userChoice] == 1){
                System.out.println("You win a brand new car!");
                firstWin++;
            }
            else if(doors[swapChoice] == 1){
                System.out.println("You win a brand new car!");
                changedAnswer++;
            }
            else {
                System.out.println("Thats a goat!");
            }
        }
        
        displayResults(firstWin, changedAnswer);
    }
    
    public static int[] setGame(){
      int[] doors;
      int carIndex;

      doors = new int[3];
      Random r = new Random();
      carIndex = r.nextInt((3));
      doors[carIndex] = 1;

      return doors;
    }
    
    public static int getChoice() {
        System.out.println("Enter a door number: 1, 2, or 3");
        Random r = new Random();
        int userChoice = r.nextInt(3);
        return userChoice;
    }
    
    public static int getMonty(int[] doors, int userChoice) {
        Random r = new Random();
        int montyChoice;
        while (true) {
            montyChoice = r.nextInt(3);
            if (montyChoice == userChoice) {
                continue;
            }
            if (doors[montyChoice] == 1) {
                continue;
            }
            break;
        }
        return montyChoice;
    }

    public static int getSwap(int userChoice, int montyChoice) {
        return 3 - userChoice - montyChoice;
    }
    
    public static void displayResults(int firstWin, int changedAnswer) {
        System.out.println("Picked on the first try: " + firstWin);
        System.out.println("Picked different door: " + changedAnswer);
    }
}
