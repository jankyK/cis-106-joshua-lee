# README #

### What is this repository for? ###
I was not able to introduce myself properly to the class so instead I will introduce myself to you Professor. My name is Josh and I took this class once already and am retaking it to replace my grade and bolster my GPA. 
I wish to reinforce my understandings of the basics of programming and at the same time work on my time management. I really look forward to discussing ideas with other students contributing to a larger conversation.
I'll be using Java as I am interesting in learning it.
Thank you for having me in this class one more time.

Session 1
-This week we learned about the basics of hardware and software and how they work together to carry out programs and tasks designated
by the user. Flowcharting and Pseudocode are useful tools that are used so that programmers can think about each step required to create the
program in mind. Getting back to basics this week reallys helps me get back into the mindset required for programming.

Session 2
-This session goes more into depth about IDE's and source code management. I personally used cloud9 since I have previous experience with it
and am aware of the benefits and drawbacks included. Source code management helps keep the program looking clean and describes the purpose of the program
so other programmers inspecting the code can see the description of the program.

Session 3/4
-This session covered a lot more about syntax and variables. Variables can be useful to store data to be modified or used in other sections of code. Relearning Java has been somewhat difficult as I don't remember all the syntax needed
to properly head modules or even declare my main. I didn't implement comments into my code this week, but the use of this is to help others or even oneself to better understand the code or its purpose.
It's been a tough week but I am struggling to continue to better my efforts in this class.

Session 5/6
-This session covered modules or subroutines. These can be used to perform one task and perform them well. I struggled with really separating the seperate functions for one task.
I think applying subroutines can be really helpful especially if combined with if statements to perform seperate tasks in the subroutine. I definitely put more time in this week into
contributing to discussions and overall participation but I still have a ways to go.

Session 4? (Assignments 7 and 8)
-This week was all about conditional statements and learning how to structure the code using such statements. This week I gained a lot of insight as to how I should be thinking when programming and how I should
approach different tasks. Approaching last week's assignments with the twist of conditional statements requires a different way of thinking and I had to sit back to actually think about what I was trying to achieve with
each statement. 
Being able to use flowgorithm in the first assignment helped me a lot because I was able to visually see which blocks of code I was at in the debugging process. Assignment 10, sepcifically the second activity I chose was very
difficult to do because I could not figure out how to correctly guess within boundries, or even get the high/low to work properly. I still haven't been able to use loops to their full extent but will continue to build off my experiences
in this week's activities.

Session 5 (Assignments 9 and 10)
-This week we leaned about loops and how they benefit our programs. Personally, I enjoyed rehashing some of these activities to include loops that simplify certain aspects of the code overall. My main issue was figuring out which loop
to use, and I've learned that it could depend on the situation. For loops seem to be the easiest as boolean values have yet to click fully in my head. Once it does, then hopefully I'll be able to implement more loops in my code.

Session 6 (Assignment 11 and 12)
-This week was about arrays and how we can implement them into our code to facilitate storing data. I really wish I had more time to experiment with this week's activities but I just did my best with what little time I had in order to 
use the arrays as to the best of my abilities. In the future I can see this coming in handy not just for calendars but also if one wanted to make a record holder that holds multiple instance of data, all for one person, a parallel array would 
come in very handy.

Session 7 (Assignment 13 and 14)
-This week we learned how to read from data files and parsing strings. This is definitely useful for when data input is needed in large batches or the user needs a file analyzed. I really struggled with the data files and learning how to parse
strings in Java was somewhat difficult but has been a great learning experience. The activities this week helped me become versed in parsing strings, data files line by line , and parsing based on different characters. Combining this weeks lessons on 
parsing and arrays is extremely useful as we can now build a database of the user's information.

Session 9 (Final Project)
-This was our final week in the course and it proved very difficult. I struggled with learning how to use Java more proficiently and even RegEx to parse and scrape data from the website. This project really was a culmination of all the topics we learned over the weeks,
from arraylists to nodes and elements. I really enjoyed working off the feedback you gave me and improving my code to become more modular and flexible as well as becoming more elegant. I really struggled with the Regex but it also proves itself very useful
if I ever need to obtain data from a website while removing irrelevant information.