//This program takes input from the user in getDistance. In getDistance, convertDistance is called which
//uses the input from getDistance as a parameter to calculate kilometers, meters, and centimeters. After
//doing so, outputResults is called which displays all the information for the user.
import java.util.*;
import java.lang.Math;

public class JavaApplication {
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        double miles;
        double km;
        double meters;
        double cm;
        
        miles = getDistance();
        km = convertKilometers(miles);
        meters = convertMeters(miles)
        cm = convertCentimeters(miles)
        outputResults(miles, km, meters, cm);
    }
    
    public static void getDistance() {
        double miles;
        
        System.out.println("Enter distance in miles: ");
        miles = input.nextDouble();
        return miles;
    }
    
    public static void convertKilometers(double miles) {
        double Km;
        
        Km = miles * 1.60934;
        return Km;
    }
    public static void convertMeters(double miles) {
        double Meters;
        
        Meters = miles * 1609.24;
        return Meters;
    }
    public static void convertCentimeters(double miles) {
        double Centimeters;
        
        Centimeters = miles * 160934;
        return Centimeters;
    }
    
    public static void outputResults(double miles, double kilometers, double meters, double centimeters) {
        System.out.println("Miles: " + miles);
        System.out.println("Kilometers: " + kilometers);
        System.out.println("Meters: " + meters);
        System.out.println("Centimeters: " + centimeters);
    }
}
