//This code asks the user for input for hourly rate and hours worked in one method. Then from that method,
//it calls another method to compute the amount earned. The last method to output the total is called from the second method
//and then outputs the actual amount earned.
import java.util.*;
import java.lang.Math;

public class JavaApplication {
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        double hours;
        double rate;
        double weeklyPay;
        
        hours = getHours();
        rate = getRate();
        weeklyPay = computeGross(hours, rate);
        outputPay(weeklyPay);
    }
    
    public static void computeGross(double hours, double hourlyRate) {
        double grossPay;
        
        grossPay = hours * hourlyRate;
        return grossPay;
    }
    
    public static void getHours() {
        double hours;
        System.out.println("Enter amount of hours worked: ");
        hours = input.nextDouble();
        //computeGross(hours, hourlyRate);
        return hours
    }
    public static void getRate() {
        double hourlyRate;
        System.out.println("Enter amount paid hourly: ");
        hourlyRate = input.nextDouble();
        return hourlyRate;
    }
    
    public static void outputPay(double grossPay) {
        System.out.println("Total earned for the week: $" + grossPay);
    }
}
